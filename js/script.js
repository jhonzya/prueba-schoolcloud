$(document).ready(function(){

	if (window.location.pathname == "/carrito/index.php" || window.location.pathname == "/carrito/") {
		iniciar();
		$.ajax({
			type: 'GET',
			url: 'api/productos.php',
			success: function(response){
				respuesta = JSON.parse(response);
				$.each(respuesta, function(i, valor){
					$('#lista_productos li:last').after('<li class="list-group-item">' +
						'<div class="checkbox">' +
							'<label>' +
								'<input type="checkbox" id="'+valor.id+'" value="'+valor.id+'" name="productos[]">' +
								valor.nombre + ' <strong>$' + valor.precio + '</strong>' +
							'</label>' +
						'</div>' +
						'</li>');
				});
			},
			error: function(e){
				console.log("Error: "+e);
			}
		});
	}

	if (window.location.pathname == "/carrito/categorias.php") {
    	$.ajax({
    		type: 'GET',
    		url: 'api/categorias.php',
    		success: function(response){
    			respuesta = JSON.parse(response);
    			$.each(respuesta, function(i, valor){
    				$('#categorias_tabla tbody tr:last').after('<tr>' +
						'<td>'+valor.id+'</td>' +
						'<td>'+valor.nombre+'</td></tr>');
    			});
    		},
    		error: function(e){
    			console.log("Error: "+e);
    		}
    	});
	}

	if (window.location.pathname == "/carrito/productos.php") {
    	$.ajax({
    		type: 'GET',
    		url: 'api/categorias.php',
    		success: function(response){
    			respuesta = JSON.parse(response);
    			$.each(respuesta, function (i, valor) {
				    $('#categorias_select').append($('<option>', { 
				        value: valor.id,
				        text : valor.nombre 
				    }));
				});
    		},
    		error: function(e){
    			console.log("Error: "+e);
    		}
    	});

		$.ajax({
			type: 'GET',
			url: 'api/productos.php',
			success: function(response){
				respuesta = JSON.parse(response);
				$.each(respuesta, function(i, valor){
					$('#productos_tabla tbody tr:last').after('<tr id="'+valor.id+'">' +
						'<td>'+valor.id+'</td>' +
						'<td>'+valor.nombre+'</td>' +
						'<td>'+valor.precio+'</td>' +
						'<td>'+valor.categoria+'</td>' +
						'<td>' +
							'<button data-id="'+valor.id+'" data-nombre="'+valor.nombre+'" data-precio="'+valor.precio+'" data-categoria="'+valor.categoria_id+'" type="button" class="btn btn-warning btn-sm editar-producto"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>' +
							'<button data-id="'+valor.id+'" type="button" class="btn btn-danger btn-sm borrar-producto"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>' +
						'</td></tr>');
				});
			},
			error: function(e){
				console.log("Error: "+e);
			}
		});
	}

	if (window.location.pathname == "/carrito/graficas.php") {
		$.ajax({
			type: 'GET',
			url: 'api/productos_charts.php',
			success: function(response){
				respuesta = JSON.parse(response);
				$('#container').highcharts({
					chart: {
						type: 'bar'
					},
					title: {
						text: 'Productos'
					},
					xAxis: {
						categories: respuesta.productos
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Ventas individuales'
						}
					},
					legend: {
						reversed: true
					},
					plotOptions: {
						series: {
							stacking: 'normal'
						}
					},
					series: [{
						name: 'En lista (carrito)',
						data: respuesta.lista
					},{
						name: 'Vendidas',
						data: respuesta.vendidos
					}]
				});
			},
			error: function(e){
				console.log("Error: "+e);
			}
		});
	}


	$('#categorias').submit(function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: 'api/guardar_categoria.php',
			data: $(this).serialize(),
			success: function(response){
				respuesta = JSON.parse(response);
				$.each(respuesta, function(i, valor){
					$('#categorias_tabla tbody tr:last').after('<tr>' +
						'<td>'+valor.id+'</td>' +
						'<td>'+valor.nombre+'</td></tr>');
				});
				$("#nombre").val("");
			},
			error: function(e){
				console.log("Error: "+e);
			}
		});
	});

	$('#productos').submit(function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: 'api/guardar_producto.php',
			data: $(this).serialize(),
			success: function(response){
				respuesta = JSON.parse(response);
				if(respuesta.editado){
					$.each(respuesta.resultados, function (i, valor) {
						$("#"+valor.id).html('<td>'+valor.id +
							'<td>'+valor.nombre+'</td>' +
							'<td>'+valor.precio+'</td>' +
							'<td>'+valor.categoria+'</td>' +
							'<td>' +
								'<button data-id="'+valor.id+'" data-nombre="'+valor.nombre+'" data-precio="'+valor.precio+'" data-categoria="'+valor.categoria_id+'" type="button" class="btn btn-warning btn-sm editar-producto"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>' +
								'<button data-id="'+valor.id+'" type="button" class="btn btn-danger btn-sm borrar-producto"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>' +
							'</td>');
					});
				}
				else{
					$.each(respuesta.resultados, function(i, valor){
						$('#productos_tabla tbody tr:last').after('<tr id="'+valor.id+'">' +
							'<td>'+valor.id+'</td>' +
							'<td>'+valor.nombre+'</td>' +
							'<td>'+valor.precio+'</td>' +
							'<td>'+valor.categoria+'</td>' +
							'<td>' +
							'<button data-id="'+valor.id+'" data-nombre="'+valor.nombre+'" data-precio="'+valor.precio+'" data-categoria="'+valor.categoria_id+'" type="button" class="btn btn-warning btn-sm editar-producto"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>' +
							'<button data-id="'+valor.id+'" type="button" class="btn btn-danger btn-sm borrar-producto"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>' +
							'</td></tr>');
					});
				}

				$("#nombre").val("");
				$("#precio").val("");
				$('#id_producto').val("");
			},
			error: function(e){
				console.log("Error: "+e);
			}
		});
	});

	$(document).on('click', '.editar-producto', function (event) {
		$("#nombre").val($(this).data('nombre'));
		$("#precio").val($(this).data('precio'));
		$("#categorias_select").val($(this).data('categoria'));
		$('#id_producto').val( $(this).data('id') );
	});

	$(document).on('click', '.borrar-producto', function (event) {
		id =  $(this).data('id');
		$.ajax({
			type: 'POST',
			url: 'api/borrar_producto.php',
			data: {
				id: id
			},
			success: function(response){
				$('#'+response).remove();
			},
			error: function(e){
				console.log("Error: "+e);
			}
		});
	});

	$('#carrito').submit(function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: 'api/agregar_carrito.php',
			data: $(this).serialize(),
			success: function(response){
				respuesta = JSON.parse(response);
				iniciar();
			},
			error: function(e){
				console.log("Error: "+e);
			}
		});
	});

	$('#boton_comprar').on('click',function (e) {
		e.preventDefault();
		console.log("comprar");

		var win = window.open('comprar.php', '_blank');
		setTimeout(function(){
			location.reload();
		}, 2000);
	})
	
	function iniciar() {
		$('#boton_comprar').addClass('disabled');
		$.ajax({
			type: 'GET',
			url: 'api/productos_carrito.php',
			success: function(response){
				respuesta = JSON.parse(response);
				$('#productos_carrito tbody tr:not(:first)').empty();
				$.each(respuesta.productos, function(i, valor){
					$('#productos_carrito tbody > tr:last').after('<tr>' +
						'<td>'+valor.nombre+'</td>' +
						'<td>'+valor.precio+'</td>' +
						'</tr>');
				});
				$('#total').html('$'+respuesta.total);
				if(respuesta.productos.length)
					$('#boton_comprar').removeClass('disabled');

			},
			error: function(e){
				console.log("Error: "+e);
			}
		});
	}
});