<?php
    require('fpdf.php');
    include 'api/vars.php';

    $sql = "SELECT * FROM carrito WHERE status = 0";
    $resultado = $conexion->query($sql);

    if(  $resultado->num_rows > 0){
        $row = mysqli_fetch_array($resultado);
        $id_carrito = $row['id'];

        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(190,10,'Ticket de compra', 0, 1, C);

        $pdf->SetFont('Arial','',12);
        $pdf->Cell(190,10,'-------------------------------------------------', 0, 1, C);

        $sql = "SELECT producto.precio as precio, producto.nombre as nombre, carrito.total as total FROM carrito, carrito_producto, producto WHERE carrito_producto.carrito_id = carrito.id AND carrito_producto.producto_id = producto.id AND carrito.id = $id_carrito";
        $productos_carrito = $conexion->query($sql);

        $suma = 0;
        while ($row = mysqli_fetch_array($productos_carrito)){
            $suma += $row['precio'];
            $pdf->Cell(0,10, $row['nombre'].' - $'.$row['precio'], 0 , 1, C);
        }
        $pdf->Cell(190,10,'-------------------------------------------------', 0, 1, C);
        $pdf->SetFont('Arial','B',14);
        $pdf->Cell(190,10,'Total: $'.$suma, 0, 1, C);

        $sql = "UPDATE carrito SET total = '$suma', status = 1 WHERE id = ".$id_carrito;
        $conexion->query($sql);

        $pdf->Output();
    }
    else{
        echo "No hay nada en el carrito";
    }