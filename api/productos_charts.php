<?php
    include 'vars.php';
    $sql = "SELECT * FROM producto";
    $resultado = $conexion->query($sql);

    $productos = array();
    $lista = array();
    $vendidos = array();
    while ($row = mysqli_fetch_array($resultado)){
        array_push($productos, $row['nombre']);

        $sql = "SELECT COUNT(*) as lista FROM carrito_producto, carrito WHERE producto_id = ".$row['id']." AND carrito.status = 0 AND carrito_producto.carrito_id = carrito.id ";
        $res = $conexion->query($sql);
        $rowL = mysqli_fetch_array($res);
        array_push($lista, (int)$rowL['lista']);

        $sql = "SELECT COUNT(*) as vendidos FROM carrito_producto, carrito WHERE producto_id = ".$row['id']." AND carrito.status = 1 AND carrito_producto.carrito_id = carrito.id ";
        $res = $conexion->query($sql);
        $rowV = mysqli_fetch_array($res);
        array_push($vendidos, (int)$rowV['vendidos']);
    }

    echo json_encode([
        'productos' => $productos,
        'lista' => $lista,
        'vendidos' => $vendidos
    ]);