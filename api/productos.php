<?php
    include 'vars.php';

    $sql = "SELECT categoria.nombre as categoria, producto.* FROM producto, categoria WHERE producto.categoria_id = categoria.id ORDER BY producto.id";
    $resultado = $conexion->query($sql);

    $resultados = array();
    while ($row = mysqli_fetch_array($resultado))
        array_push($resultados, $row);

    echo json_encode($resultados);