<?php require("header.html") ?>

    <div class="page-header">
  		<h1>Productos</h1>
	</div>

	<div class="row">
		<div class="col-md-4">
			<form id="productos">
				<div class="form-group">
					<label>Categoría</label>
					<select class="form-control" id="categorias_select" name="categoria"></select>
				</div>
				<div class="form-group">
					<label for="nombre">Nombre</label>
					<input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" required>
				</div>
				<div class="form-group">
					<label for="nombre">Precio</label>
					<input type="text" class="form-control" id="precio" placeholder="Precio" name="precio" required>
				</div>
				<input type="hidden" name="id_producto" id="id_producto">
				<button type="submit" class="btn btn-default">
					Guardar
				</button>
			</form>
		</div>

		<div class="col-md-8">
			<table class="table" id="productos_tabla">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nombre</th>
						<th>Precio</th>
						<th>Categoría</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				<tr></tr>
				</tbody>
			</table>
		</div>
	</div>

<?php require("footer.html") ?>