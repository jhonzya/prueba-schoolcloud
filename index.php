<?php require("header.html") ?>

    <div class="page-header">
        <h1>Carrito</h1>
    </div>

    <div class="row">
        <div class="col-md-8">
            <form id="carrito">
                <ul class="list-group" id="lista_productos">
                    <li class="list-group-item active">
                        Productos
                    </li>
                </ul>
                <button type="submit" class="btn btn-primary">Agregar</button>
            </form>
        </div>
        <div class="col-md-4 well">
            <h4>Mis productos</h4>
            <table id="productos_carrito" class="table">
                <thead>
                    <th>Nombre</th>
                    <th>Precio</th>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
            <h4 id="total" class="text-right"></h4>
            <a href="comprar.php" class="btn btn-block btn-primary disabled" id="boton_comprar">Comprar</a>
        </div>
    </div>


<?php require("footer.html") ?>