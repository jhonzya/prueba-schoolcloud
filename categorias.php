<?php require("header.html") ?>

    <div class="page-header">
  		<h1>Categorías</h1>
	</div>

	<div class="row">
		<div class="col-md-4">
			<form id="categorias">
				<div class="form-group">
					<label for="nombre">Nombre</label>
					<input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" required>
				</div>
				<button type="submit" class="btn btn-default">
					Guardar
				</button>
			</form>
		</div>

		<div class="col-md-8">
			<table class="table" id="categorias_tabla">
				<thead>
					<tr>
						<td>ID</td>
						<th>Nombre</th>
					</tr>
				</thead>
				<tbody>
					<tr></tr>
				</tbody>
			</table>
		</div>
	</div>


<?php require("footer.html") ?>